"""
! = Element
$ = Property
& = Event
? = Operation
"""


# === Minimal Event Stream ===
stream.new_element("element")
stream.new_event("event")

stream.get_property("property").attach_to_element("element", "operation_1", 1.)
stream.get_operation("operation_1").attach_to_event("event")
#
stream.get_element("element").get_property("property").mult("operation_2", 0.)
stream.get_operation("operation_2").attach_to_event("event")


# === Temperature change ===
# There is an element called Weather...
stream.new_element("weather")
stream.new_event("day_1")
stream.new_event("temperature_change_a")
stream.new_event("temperature_change_b")
stream.new_event("termometer_reading_a")
stream.new_event("termometer_reading_b")

# ...with a Property called Temperature...
stream.get_property("celcius_temperature").attach_to_element("weather", "operation_1", 10)
stream.get_operation("operation_1").attach_to_event("day_1")

# ...Ana read the temperature, it was 20...
stream.get_element("weather").get_property("celcius_temperature").sum("operation_2", 20.)
stream.get_operation("operation_2").attach_to_event(["day_1", "temperature_change_a"])
stream.get_event("temperature_change_a").set_observation("termometer_reading_a")

# ...Laura also read the temperature, it was 40...
stream.get_element("weather").get_property("celcius_temperature").sum("operation_3", 40.)
stream.get_operation("operation_3").attach_to_event(["day_1", "temperature_change_b"])
stream.get_event("temperature_change_b").set_observation("termometer_reading_b")

# ...But I trust more Ana than Laura...
stream.get_event("termometer_reading_a").set_confidence(0.8)
stream.get_event("termometer_reading_b").set_confidence(0.2)

print(stream.get_element("weather").get_property("celcius_temperature").get_value())


# === Ownership change ===
# There was an apple
stream.new_element("apple")

# And Lucas and John
stream.new_element("lucas")
stream.new_element("john")

# Lucas took the apple
stream.get_element("apple").parent("lucas", 1.)

# and give it to John
stream.get_element("apple").parent("lucas", 0.)
stream.get_element("apple").parent("john", 1.)


# === Copy ===
# There was a file
stream.new_element("file_1")

# and the file was copied
stream.copy_element("file_1", "file_2")


# === Instance ===
# There was a human
stream.new_element("human_1")

# and another human
stream.instance_element("human_1", "human_2")

# === Global to local ===
# There was a human
stream.new_element("human_1")

# the human was part of a crowd
stream.new_element("crowd")
stream.get_element("human_1").part_of("crowd", 1.)

# the crowd is standing
stream.new_property("standing").attach_to_element("crowd", "operation_1", 1)

# Is the human standing?
print(stream.get_element("human_1").get_property("standing").get_value())


# === Local to global ===
# There was three humans
stream.new_element("human_1")
stream.new_element("human_2")
stream.new_element("human_3")

# Some where standing, some not
stream.new_property("standing").attach_to_element("human_1", "operation_1", 1)
stream.new_property("standing").attach_to_element("human_2", "operation_2", 0)
stream.new_property("standing").attach_to_element("human_3", "operation_3", 1)

# They were part of a crowd
stream.new_element("crowd")
stream.get_element("human_1").part_of("crowd", 1.)
stream.get_element("human_2").part_of("crowd", 1.)
stream.get_element("human_3").part_of("crowd", 1.)

# Is the crowd standing?
print(stream.get_element("crowd").get_property("standing").get_value())


# === Movie awards ===
# There was three awards and a movie
stream.new_element("award_1")
stream.new_element("award_2")
stream.new_element("award_3")
stream.new_element("movie")

# the movie won two awards
stream.get_element("award_2").parent("movie", 1.)
stream.get_element("award_3").parent("movie", 1.)

# How many awards the movie have?
movie_awards = stream.get_element("movie").get_childrens(similar_to="award_1")
print(len(movie_awards))

# Total awards for a movie
# El premio se especifica cambiando el propietario del premio
# de "La Academia" a la "Película"
# El dueño se especifica con un vector multidimencional y otro valor
# que dice el nivel de adueñamiento.

# Cada elemento, propiedad y evento tienen un ID único
# en forma de vector multidimencional
