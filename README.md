# Stochastic Multiresolution Event Function Flow for Structured Knowledge Representation

Caraballo, G.

## Technical field
La presente invención se refiere al campo de la Ciencia de la Computación, más específicamente al área de la Inteligencia Artificial.

## Abstract
This paper presents a novel Knowledge Representation inspired on Event Cognition models, capable to store experiences in a similar way our brain might do. This representation have several advantages, including the capability of storing factual data, process descriptions and historical changes in a unified format. Its stochastic and multiresolution nature allows to deal with contradictory and missing information. It is differentiable, allowing to easily integrate the stored information to Machine Learning techniques based on gradient descent. Since its main goal is to be a general description of the world it can also work as a Universal Dataset including every known dataset in a single file.

## 1 Introduction
Being able to store and manipulate knowledge in a way that software can understand have being a challenge for decades.

A novel technique for Knowledge Representation is presented using a flow of tensors manipulated by time-ordered functions.

## 2 Background
Los avances mas importantes en el modelado de flujos de eventos se han dado en el campo de la Ontología de Procesos, aplicado principalmente a procesos de manufactura, ingeniería o procesos empresariales.
En cuanto al estudio de los eventos como base para el proceso y almacenamiento de la información referimos al Event Horizon Model.
Por otro lado las representaciones del conocimiento como método de describir el mundo están mas bien normalizadas y comparten una estructura similar, describiendo Individuos, Clases, Atributos y Relaciones.
Éstas representaciones del conocimiento son extremadamente rígidas, requiriendo una caracterización previa de las Clases, Atributos y Relaciones. Y su poder descriptivo se limita a modelar un único estado del conocimiento, sin tener en cuenta su variación a lo largo del tiempo, y sin tener en cuenta posibles contradicciones o imprecisiones.
Nótese que varias de éstas limitaciones se pueden sortear gracias a la plasticidad de éstos métodos, pero resultan en representaciones del conocimiento extremadamente complejas, difíciles de utilizar y mantener.
Por lo tanto se propone una Representación del Conocimiento que no requiere una definición previa de Clases ni Relaciones, capaz de dar cuenta de las variaciones del conocimiento a lo largo de un eje temporal, y de dar cuenta de contradicciones e imprecisiones. A partir de modelar el conocimiento como un flujo de eventos.

- Knowledge Representation:
- Event Cognition:
- Factual Data:
- Process descriptions:
- Historical changes:

## 3 Related Work
Event Cognition

## 4 Stochastic Multiresolution Event Function Flow
The Knowledge Representation consist of Properties, Elements, Operations and Events. The Properties from different Elements are modified through Operations organized on different Events.

- Every Property can be owned by one or more Elements
- Properties are modified by Operations
- Every Operation can be owned by one or more Events
- Events can be observations of other Events
- A Property is a scalar paired with a confidence value ranging from 0 to 1 and a multiplication factor
- An Element is only a container for Properties
- An Event is only a container for Operations

Updated:

Se provee una Representación del Conocimiento conformada de Operaciones, Propiedades, Elementos y Eventos, y su relación entre estos ítems. Esta Representación del Conocimiento es capaz describe cómo las Propiedades de diferentes Elementos son modificadas a través de Operaciones organizadas en Eventos.

- Cada Propiedad puede pertenecer a uno o más Elementos.
- Las Propiedades son modificadas a través de Operaciones.
- Cada Evento es un conjunto de una o más Operaciones.
- Un Evento puede ser la observación de uno o mas Eventos.
- Un Evento puede ser instancia de uno o mas Eventos.
- Cada Operación puede pertenecer a uno o más Eventos.
- Un Elemento puede ser parte de uno o mas Elementos.
- Un Elemento puede ser hijo de uno o mas Elementos.
- Un Elemento puede ser instancia de uno o mas Elementos.
- Un Elemento puede ser una copia (duplicado) de otro Elemento.

Sólo estos ítems son suficientes para generar una representación expresiva, flexible y potente del conocimiento.
Cada Elemento, Propiedad, Operación y Evento es único e identificable. Hay diferentes tipos de instancia y copia (por ejemplo una copia simétrica)
Un elemento puede definirse de diferentes maneras, y las definiciones pueden variar.


## Misc notes

Ontología basada en Eventos

Hay un stream de vectores modificados por eventos
Los vectores representan parámetros en el sistema, el Yo, otros, etc.

Para modelar metas se pueden usar “eventos de demostración” que ejemplifican la transformación deseada, aunque no quede claro de que manera se logra.

La relación causa y efecto no está modelado en el sistema, sino que es algo que debe calcularse probabilisticamente.

Los eventos se pueden instanciar para generar nuevos eventos, se utiliza ponderación para mezclar diferentes eventos en uno.

Las relaciones también surgen de los eventos, y nos son intrínsecamente parte de ellos. Una persona es madre de otra porque hubo un “nacimiento”, no porque esa persona sea madre.

Los eventos tienen “testigos”, y de esa manera podemos modelar el conocimiento que tienen los otros. Si un otro fue testigo de un evento, entonces lo conoce tal como nosotros.

Los eventos pueden ser observados utilizando diferentes sentidos en simultaneo o por separado. Algunos sentidos pueden no estar disponibles en ciertos eventos o en ciertos momentos.

Colores: espectogramas
Musica: notas

El tiempo se indica a través de Restricciones (antes de, en tal momento, después de)

“cogni-tive work can be offloaded onto the environment and that the environment is part of the cognitive system. So, people do not keep information active in the mind if it is available externally.”

Isomorfismo: Representar utilizando una estructura semejante a lo representado. (Elementos virtuales?)

El efecto humoristico se produce al saltar bruscamente de un evento a otro, o al describir detalles de eventos (sobre todo eventos de los que no se habla)

El acto creativo se da al verse forzado en agregar propiedades a un evento cuando no las tiene.

Terror: Es cuando los eventos no resultan como esperamos.

“The event indexing model assumed that there are at least five dimensions that people keep track of during comprehension, namely, space, time, causality, intentionality, and entity.”

El tiempo es dinámico y recursivo
representación de color: Histograma sobre el espectograma
La personalidad es como actúa cada persona ante una situación
Que pasa cuando un evento se da por la ausencia de algo?
Que pasa si el quien, cuando, donde o porque de un evento son desconocidos?

machine learnin puede ser usado para encontrar patrones en la representacion mental

Anyone can cook ("Anyone" puede cocinar)
The money is not on the table ("No Money" is on the table)

Método para la descripción de eventos en un lenguaje formal

Método y sistema para el modelado de flujos de eventos

Event-driven process chain

Embesbings para modelar auto recorriebndo calles

Todo es una observación. Musica, sonidos, efectos
Cómo modelar el olvido?
"El problema del amigo vegetariano"
Hablar con uno mismo

Saltar entre diferentes tipos de representación de los datos

Moving from Dataset to Memory

Bayesian: Subjetivistas

Open World Assumption (Verdadero, Falso y Desconocido)

Node Skipping and Node Correlation

Granger Causality

Web Enabled Temporal Analysis Systems (WebTAS) semi-natural language (SNL)

Crisp: Non Fuzzy

Information Decay

Markov Random Fields

CP-theories

Faceted classification

Structured Knowledge Representation Model
